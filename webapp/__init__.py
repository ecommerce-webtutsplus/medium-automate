#!/usr/bin/python3
from scraper.scraper.storage.flask_datalayer import get_colleges, get_all_colleges, create_articles, get_cities, get_states, get_college, update_college, db

from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask_cors import CORS, cross_origin
from scraper.scraper.storage.models import DATABASE_URI


def format_currency(amount, currency):
    if not amount:
        return None
    return '{} {:,.2f}'.format(currency.name, amount)


def pretty_none(value):
    if value is None:
        return ''
    return value


def yes_if_true(value, otherwise=''):
    if value is True:
        return 'Yes'
    return otherwise


def dl_item(key, value):
    if not value:
        return ''
    return '<div><dt>{}:</dt><dl>{}</dl></div>'.format(key, value)


app = Flask(__name__, template_folder='templates/')
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.jinja_env.globals.update(format_currency=format_currency)
app.jinja_env.globals.update(pretty_none=pretty_none)
app.jinja_env.globals.update(yes_if_true=yes_if_true)
app.jinja_env.globals.update(dl_item=dl_item)

db.init_app(app)

@cross_origin()
@app.route('/add-articles', methods=['POST'])
def add_articles():
    print(request.get_json());
    url = request.get_json()['url']
    create_articles(url);
    return {"created": True}

if __name__ == '__main__':
    app.run(debug=True)
