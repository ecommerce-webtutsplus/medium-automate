# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import json
from pprint import pprint

from sqlalchemy import Integer, Float, Boolean, String
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker

from .storage.models import create_connection, create_tables, College, CollegeFeeCurrencyEnum, CollegeTypeEnum

type_dict = {
    'college': College,
}


mul_dict = {
    'K': 1000,
    'Lacs': 100000,
    'Million': 1000000,
    'Crore': 10000000,
}


def process_fee_range(fee_range):
    currency = CollegeFeeCurrencyEnum.INR
    if fee_range.strip()[:3] == 'USD':
        currency = CollegeFeeCurrencyEnum.USD
        fee_range = fee_range.replace('USD', '')
    else:
        fee_range = fee_range.replace('INR', '')

    fee_range.strip()
    fee_range = [i.strip() for i in fee_range.split('-')]

    for i, v in enumerate(fee_range):
        val, mul = v.split()
        fee_range[i] = float(val) * mul_dict[mul]

    start = fee_range[0]
    end = fee_range[1] if len(fee_range) > 1 else fee_range[0]

    return start, end, currency


class ScraperPipeline:
    def __init__(self):
        engine = create_connection()
        create_tables(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        # For now, we are only scraping colleges (and yielding it) and nothing else.
        # So we directly get the data from the dict since it is guaranteed to be an
        # instance of the College class.
        item = item['data']

        # Preprocess (e.g. split the `fee_range` field)
        fee_range = item.get('fee_range')
        if fee_range:
            item['fee_start'], item['fee_end'], item['fee_currency'] = process_fee_range(fee_range)
        if 'fee_range' in item:
            del item['fee_range']

        # Prepare for MySQL (type casting).
        if item.get('g_ratio'):
            ratio = item['g_ratio']
            if ':' in ratio:
                a, b = [float(i) for i in ratio.split(':')]
            elif ';' in ratio:
                a, b = [float(i) for i in ratio.split(';')]
            else:
                a, b = float(ratio), 1
            item['g_ratio'] = a/b

        if item.get('rating'):
            item['rating'] = float(item['rating'].replace('width:', '').replace('%', '').strip())

        if item.get('n_reviews'):
            item['n_reviews'] = int(item['n_reviews'].split()[0])

        if item.get('type'):
            college_type = item['type'].strip()
            if college_type == 'Public':
                item['type'] = CollegeTypeEnum.PUBLIC
            elif college_type == 'Government':
                item['type'] = CollegeTypeEnum.GOVERNMENT
            elif college_type == 'State University':
                item['type'] = CollegeTypeEnum.STATE
            elif college_type == 'Central University':
                item['type'] = CollegeTypeEnum.CENTRAL
            elif college_type == 'Private':
                item['type'] = CollegeTypeEnum.PRIVATE
            elif college_type == 'Deemed to be University':
                item['type'] = CollegeTypeEnum.DEEMED_TO_BE_UNI
            elif college_type == 'Deemed University Private':
                item['type'] = CollegeTypeEnum.DEEMED_PRIVATE

        for k, v in College.__table__.c.items():
            if not item.get(k) or k in ['fee']:
                continue

            if isinstance(v.type, Integer):
                item[k] = int(item[k])
            elif isinstance(v.type, Float):
                item[k] = float(item[k])
            elif isinstance(v.type, Boolean):
                item[k] = item[k] == 'Available'

        db = self.Session()
        try:
            college = College(**item)
            db.add(college)
            db.commit()
        except IntegrityError:
            # TODO: Don't scrape already known colleges.
            db.rollback()
        except:
            db.rollback()
            raise
        finally:
            db.close()

        return item
