from urllib.parse import urlencode

import scrapy


def process_info(college):
    infos = college.css('ul.info > li::text').extract()

    rv = {}
    for info in infos:
        key, value = info.split(':')
        values = value.splitlines()
        if len(values) > 1:
            value = [i.strip() for i in values if i.strip()]
        else:
            value = value.strip()
        rv[key] = value

    return rv


class CollegedekhoSpider(scrapy.Spider):
    name = 'collegedekho'
    allowed_domains = ['collegedekho.com']
    start_urls = ['https://www.collegedekho.com/medical/colleges-in-india/']

    next_index_params = {
        'page': 1,
        'tags': 'medical',
        'load_more': 1,
        'geo_filter': False,
    }

    college_url_specified = False

    def __init__(self, college_url=None):
        if college_url:
            self.start_urls = [college_url]
            self.college_url_specified = True

    def parse(self, response):
        self.next_index_params['csrfmiddlewaretoken'] = response.css('#csrf_token::attr(value)').get()

        if self.college_url_specified:
            yield from self.parse_college_page(response)
        else:
            yield from self.parse_index(response)

    def parse_index(self, response):
        selection = response.css('.col-md-12 .box')
        for college in selection:
            yield response.follow(college.css('.title > a::attr(href)').get(), callback=self.parse_college_page)

        if selection:
            yield self.next_index_page(response)

    def next_index_page(self, response):
        self.next_index_params['page'] += 1

        return response.follow(
            '/get_institutes_ajax/', method='POST', body=urlencode(self.next_index_params),
            callback=self.parse_index, headers={
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Referer': 'https://www.collegedekho.com/medical/colleges-in-india/',
                'Origin': 'https://www.collegedekho.com',
                'Host': 'www.collegedekho.com',
                'TE': 'Trailers',
                'X-Requested-With': 'XMLHttpRequest',
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0',
            })

    def parse_college_page(self, response):
        meta = response.css('.infraList')
        info = dict(zip(meta.css('div.label::text').extract(), meta.css('td.data::text').extract()))
        city, state = response.css('span.location::text').get().strip().replace(')', '').split(' (')
        yield {
            'type': 'college',
            'data': {
                'id': response.css('meta[name=college_id]::attr(content)').get(),
                'url': response.request.url,

                'title': response.css('h1.tooltip::text').get().strip().split(', ', 1)[0],

                'logo_url': response.css('div.collegeLogo > img::attr(src)').get(),
                'college_image_url': response.css('a.collegeImage > img::attr(data-gsll-src)').get(),
                'description': response.css('#a3 ::text').get().strip(),

                'city': city,
                'state': state,

                'type': info.get('College Type'),
                'established_in': info.get('Established In'),

                'rating': response.css('.star-ratings-sprite-rating::attr(style)').get(),
                'n_reviews': response.css('.rating-per span a::text').get(),

                'nirf': info.get('NIRF Ranking'),

                'fee_range': info.get('Fee Range'),
                # `fee_start` and `fee_end` will be handled by pipeline.
                # The pipeline will also delete `fee_range`.

                'foreign_exchange': info.get('Foreign Exchange Program'),
                'financial_assistance': info.get('Financial Assistance'),

                'n_courses': info.get('Courses Offered'),
                'total_intake': info.get('Total Intake'),

                'sf_ratio': info.get('Student Faculty Ratio'),
                'g_ratio': info.get('Gender Ratio'),

                'n_faculty': info.get('Total Faculty'),
            },
        }
