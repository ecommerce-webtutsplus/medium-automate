from flask_sqlalchemy import SQLAlchemy

from scraper.scraper.storage.models import metadata

db = SQLAlchemy(metadata=metadata)
