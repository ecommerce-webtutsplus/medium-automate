import json

from sqlalchemy import or_

try:
    from scraper.scraper.storage.models import College, MediumArticles, create_connection, create_tables
    from scraper.scraper.storage.flaskdb import db
    from scraper.scraper.storage import models
except ImportError:
    from scraper.models import College, MediumArticles
    from scraper.scraper.storage import models
    from scraper.storage.flaskdb import db

engine = models.create_connection()
models.create_tables(engine)
#Session = sessionmaker(bind=engine)


# TODO: Pagination
def get_colleges(city_state):
    if city_state:
        colleges = db.session.query(College).filter(or_(
            College.city == city_state,
            College.state == city_state,
        )).all()
        n_colleges = len(colleges)
    else:
        colleges = db.session.query(College).all()
        n_colleges = len(colleges)

    return colleges, n_colleges


def get_all_colleges():
    colleges = db.session.query(College).all()
    n_colleges = len(colleges)
    return colleges


def get_college(college_id):
    college = db.session.query(College).get(college_id)

    return college


def update_college(college_id, values):
    college = db.session.query(College).filter(College.id == college_id)
    college.update(values)
    db.session.commit()

    return college

def create_articles(url):
    article = MediumArticles()
    article.url = url;

    db.session.add(article)
    db.session.commit()

    return article


def get_cities():
    cities = db.session.query(College.city).distinct()

    return cities


def get_states():
    states = db.session.query(College.state).distinct()
    return states

